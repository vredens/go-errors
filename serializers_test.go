package errors_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	errors "gitlab.com/vredens/go-errors"
)

func TestStackOf(t *testing.T) {
	var stack []string

	e01 := fmt.Errorf("default error")
	stack = errors.StackOf(e01)
	assert.Equal(t, 1, len(stack))
	assert.Equal(t, "default error", stack[0])

	e02 := errors.Wrap(e01, "sample error")
	stack = errors.StackOf(e02)
	assert.Equal(t, 2, len(stack))
	assert.Equal(t, "sample error", stack[0])
	assert.Equal(t, "default error", stack[1])

	e03 := errors.Wrap(e02, "bad request")
	stack = errors.StackOf(e03)
	assert.Equal(t, 3, len(stack))
	assert.Equal(t, "bad request", stack[0])
	assert.Equal(t, "sample error", stack[1])
	assert.Equal(t, "default error", stack[2])
}

type someOtherError struct{}

func (err someOtherError) Error() string {
	return "erro"
}

func (err someOtherError) Code() int {
	return 10
}

func (err someOtherError) Cause() error {
	return fmt.Errorf("here be error")
}

func TestCollate(t *testing.T) {
	var str string

	str = errors.Collate(nil)
	assert.Equal(t, "", str)

	str = errors.Collate(fmt.Errorf("sample"))
	assert.Equal(t, "sample", str)

	e01 := fmt.Errorf("basic error")
	e02 := errors.Wrap(e01, "bad request 2")
	e03 := errors.Wrap(e02, "bad request 3")
	str = errors.Collate(e03)
	assert.Equal(t, "bad request 3; bad request 2; basic error", str)

	e04 := &someOtherError{}
	e05 := errors.Wrap(e04, "last error")
	str = errors.Collate(e05)
	assert.Equal(t, "last error; erro; here be error", str)
}

func TestFormatters(t *testing.T) {
	e01 := fmt.Errorf("basic error")
	e02 := errors.WithCode(errors.EDevPoo).Wrap(e01, "sample error")
	e03 := errors.WithCode(errors.EDevPoo).Wrap(e01, `{"data":["a","b"],"msg":"a json error message with çpecial chäracters @"}`)

	var strs []string

	errors.Formatter(errors.JSONFormatter)

	strs = errors.StackOf(e01)
	assert.JSONEq(t, `{"code":-1,"message":"basic error"}`, strs[0])
	strs = errors.StackOf(e02)
	assert.JSONEq(t, `{"code":5003,"message":"sample error"}`, strs[0])
	strs = errors.StackOf(e03)
	assert.JSONEq(t, `{"code":5003,"message":"{\"data\":[\"a\",\"b\"],\"msg\":\"a json error message with çpecial chäracters @\"}"}`, strs[0])

	errors.Formatter(errors.TextFormatter)

	strs = errors.StackOf(e01)
	assert.Equal(t, "[code:-1][message:basic error]", strs[0])
	strs = errors.StackOf(e02)
	assert.Equal(t, "[code:5003][message:sample error]", strs[0])
}
