package errors

import (
	"fmt"
	"sync"
)

var formatter = BasicFormatter
var mux = &sync.Mutex{}

// Formatter sets the function which converts errors to strings.
// The default formatter is ToJSON function and converts any error to a JSON string.
// Formatter is used by all methods which convert an error and the stack of errors to string or slices of strings.
// This setter is concurrency safe.
func Formatter(f func(error) string) {
	mux.Lock()
	defer mux.Unlock()
	formatter = f
}

// JSONFormatter converts an error into a JSON string with code and message for the error.
// If the code does not exist then a code of -1 is returned.
func JSONFormatter(err error) string {
	if err, ok := err.(Error); ok {
		return fmt.Sprintf("{\"code\":%d,\"message\":%q}", err.Code(), err.Error())
	}

	return fmt.Sprintf("{\"code\":-1,\"message\":%q}", err.Error())
}

// TextFormatter converts an error into a simple human readable text string.
// If the code does not exist then a code of -1 is used.
func TextFormatter(err error) string {
	if err, ok := err.(Error); ok {
		return fmt.Sprintf("[code:%d][message:%s]", err.Code(), err.Error())
	}

	return fmt.Sprintf("[code:-1][message:%s]", err.Error())
}

// BasicFormatter converts an error into a simple human readable text string.
// If the code does not exist then a code of -1 is used.
func BasicFormatter(err error) string {
	return err.Error()
}

// Collate returns a combination of the error's message and each error message in the stack of causes separated by ';'
func Collate(err error) string {
	if err == nil {
		return ""
	}

	if err, ok := err.(Error); ok {
		cause := err.Cause()
		msg := err.Error()
		for cause != nil {
			msg += "; " + cause.Error()
			if tmp, ok := cause.(Error); ok {
				cause = tmp.Cause()
			} else {
				cause = nil
			}
		}
		return msg
	}

	return err.Error()
}

// StackOf returns a list of error messages by converting the error and each error in the stack of causes to a string.
// The conversion to string is done by the configured formatter (see the Formatter function).
func StackOf(err error) []string {
	if err, ok := err.(Error); ok {
		var causes = []string{formatter(err)}
		cause := err.Cause()
		for cause != nil {
			if tmp, ok := cause.(Error); ok {
				causes = append(causes, formatter(tmp))
				cause = tmp.Cause()
			} else {
				causes = append(causes, formatter(cause))
				cause = nil
			}
		}
		return causes
	}

	return []string{formatter(err)}
}
