# go-errors

[![pipeline status](https://gitlab.com/vredens/go-errors/badges/master/pipeline.svg)](https://gitlab.com/vredens/go-errors/commits/master)
[![coverage report](https://gitlab.com/vredens/go-errors/badges/master/coverage.svg)](https://gitlab.com/vredens/go-errors/commits/master)

An opinionated error package.

Errors should have codes for easily checking types of errors. These types should be a very small list and for that reason a set of common error codes is provided which contains more than should be needed. I'm not in favour of generalized error type checking, AKA exception handling. These however, become handy when dealing with error categories such as duplicates, concurrency, bad parameters, not founds, unexpected errors (most common error form), etc. You should have mostly unexpected errors in your application and only make use of other types when you are creating a library or service.

Wrap errors! Errors should be wraped so that your package only returns errors created by your package instead of the typically lazy `return err`. This is usually done on your public methods since there is what provides more meaning to the user. 

The stack of errors should only be logged at the end of the workflow. Stack of errors is NOT a call stack. It should be much smaller and significant.

Code file and line is something useful for tests not runtime errors. When seeing an error log you should to have a general idea of what went wrong without looking at code.

## Requirements

* At least Go 1.9
* Currently tested with Go 1.10

## Installing the lib

### Global install

```
go get -u gitlab.com/vredens/go-errors
```

### Dep (vendoring)

```
dep ensure -add gitlab.com/vredens/go-errors
```

## Example

```go
package main

imports errors "go-errors"
imports fmt

var BadRequestError = errors.WithCode(errors.EBadRequest)

func myFunc1() error {
	return BadRequestError.Wrap(myFunc2(), "you did the booboo")
}

func myFunc2() error {
	return errors.Wrap(myFunc3(), "at my func %d", 2)
}

func myFunc3() error {
	return fmt.Errorf("the source of all evil")
}

func main() {
	err := myFunc1()
	if err != nil {
		// print a full error stack in a single line
		fmt.Println(errors.Collate(err))
	}

	switch err.Cause() {
		case errors.EBadRequest:
			fmt.Println("RTFM!")
		default:
			fmt.Println("oops, sorry")
	}

	causes := errors.CausesOf(err)
	for _, err := range causes {
		fmt.Println(err)
	}
}
```
