// Package errors is an opinionated error package which supports error wrapping and error codes.
package errors

import (
	"fmt"
	"io"
)

// These are sample application errors.
// They are heavily based on HTTP errors and aim at providing some guidelines to using
const (
	// EBadRequest should be used as when a request to a function is invalid and there is no other error code which makes
	// more sense.
	EBadRequest = 4000
	// EInvalidParameter should be used when a function parameter has an invalid value.
	EInvalidParameter = 4001
	// EConflict should be used when attempting to save an entity which has a duplicate with conflicting information.
	EConflict = 4090
	// ENotFound should be used when any resource does not exist. It is a generic version of the entity not found.
	ENotFound = 4040
	// EDataInconsistency should be used when the data layer contains unexpected errors or inconsistencies.
	EDataInconsistency = 5001
	// EUnexpected should be used when the error is caused by third party libraries which means that the error cause
	// should be set.
	EUnexpected = 5002
	// EDevPoo should be used when the most likely cause is developer error.
	EDevPoo = 5003
	// EConcurrencyException should be used when an operation fails due to any concurrency issues (deadlock, another
	// operation already running and can only have one, etc).
	EConcurrencyException = 5004
	// ETimeout should be used when an operation exceeds the expected time.
	ETimeout = 5005
	// EOverflow should be used when a buffer, queue, stack, etc limit is exceeded.
	EOverflow = 5006
	// EBufferFull should be used when a buffer is full on a request to add data to the buffer.
	EBufferFull = 5007
	// EBufferEmpty should be used when a buffer is empty on a request to fetch data from a buffer
	EBufferEmpty = 5008
)

// Error is a error interface with support for error code and cause stack.
// The Code should be used instead of the usual type reflection for handling different types of error.
// There is also a Cause which is not serialized but can be used to wrap original cause of errors.
// This in turn allows a logger to go through the cause stack and print the whole trace of errors.
// You should always wrap errors each time you handle them and log the error stack when the error is irrelevant for the
// downstream.
type Error interface {
	error
	Code() int
	Cause() error
}

type appError struct {
	code    int
	message string
	cause   error
	stack   bool
}

func getStack() string {
	return "stack: "
}

// New creates a new Error implementation.
// Error code is set to EUnexpected.
func New(format string, args ...interface{}) Error {
	return appError{
		cause:   nil,
		code:    EUnexpected,
		message: fmt.Sprintf(format, args...),
	}
}

// Wrap creates a new Error implementation wrapping the underlying cause specified by err.
// Error code is set to EUnexpected.
func Wrap(err error, format string, args ...interface{}) Error {
	return appError{
		cause:   err,
		code:    EUnexpected,
		message: fmt.Sprintf(format, args...),
	}
}

// Error ...
func (err appError) Error() string {
	return err.message
}

func (err appError) Format(s fmt.State, verb rune) {
	switch verb {
	case 'v':
		io.WriteString(s, Collate(err))
	case 's':
		io.WriteString(s, err.Error())
	case 'q':
		fmt.Fprintf(s, "%q", Collate(err))
	}
}

// Code returns the current error code.
func (err appError) Code() int {
	return err.code
}

// Cause returns this error's underlyng cause which in turn can be another wrapper.
func (err appError) Cause() error {
	return err.cause
}

// Encoder is an error generator for generating errors with a defined error code.
type Encoder struct {
	code int
}

// WithCode returns a new Encoder to generate Error implementations with the specified code.
// A good example is to setup your application Encoders for each of your package errors and
// use them to generate new errors with your errors codes such as BadParamError.New("invalid id").
func WithCode(code int) Encoder {
	return Encoder{code: code}
}

// New creates a new Error instance with the Encoder's error code.
func (enc Encoder) New(format string, args ...interface{}) Error {
	return appError{
		cause:   nil,
		code:    enc.code,
		message: fmt.Sprintf(format, args...),
	}
}

// Wrap creates a new Error instance with the Encoder's error code.
func (enc Encoder) Wrap(err error, format string, args ...interface{}) Error {
	return appError{
		cause:   err,
		code:    enc.code,
		message: fmt.Sprintf(format, args...),
	}
}

type coded1 interface {
	Code() int
}
type coded2 interface {
	GetCode() int
}

// CodeOf extracts an error code from a generic error.
// Attempts type assertions to interface with Code() int or GetCode() int.
// If no specific code is found a EUnexpected is returned instead.
func CodeOf(err error) int {
	if err, ok := err.(coded1); ok {
		return err.Code()
	}
	if err, ok := err.(coded2); ok {
		return err.GetCode()
	}

	return EUnexpected
}
