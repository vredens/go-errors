package errors

import "fmt"

// NewError creates a new error struct which implements the Error interface.
// Deprecated: in favor of the New method.
func NewError(code int, message string, cause error) Error {
	return appError{
		cause:   cause,
		code:    code,
		message: message,
	}
}

// NewErrorf is a wrapper to facilitate creating error messages with unstructured context data.
// Deprecated: in favor of the New method.
func NewErrorf(code int, cause error, format string, args ...interface{}) Error {
	return appError{
		cause:   cause,
		code:    code,
		message: fmt.Sprintf(format, args...),
	}
}

// NewUnexpectedError is a helper for generating an Error with error code set to ErrorUnexpected.
// Deprecated: in favor of the New method.
func NewUnexpectedError(message string, cause error) Error {
	return appError{
		cause:   cause,
		code:    EUnexpected,
		message: message,
	}
}
