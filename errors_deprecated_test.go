package errors_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	errors "gitlab.com/vredens/go-errors"
)

func TestError(t *testing.T) {
	var ok bool
	e := fmt.Errorf("basic error")

	err := errors.NewError(errors.EBadRequest, "bad request 1", nil)
	_, ok = err.(error)
	assert.True(t, ok)
	assert.Nil(t, err.Cause())
	assert.Equal(t, errors.EBadRequest, err.Code())

	err2 := errors.NewError(errors.EBadRequest, "bad request 2", e)
	_, ok = err2.(error)
	assert.True(t, ok)
	assert.Equal(t, e, err2.Cause())
	assert.Equal(t, errors.EBadRequest, err2.Code())
	assert.Equal(t, "bad request 2", err2.Error())
	assert.Equal(t, "bad request 2; basic error", errors.Collate(err2))
}

func TestErrof(t *testing.T) {
	var ok bool
	e := fmt.Errorf("basic error")

	err := errors.NewErrorf(errors.EBadRequest, nil, "sample %s", "test")
	_, ok = err.(error)
	assert.True(t, ok)
	assert.Nil(t, err.Cause())
	assert.Equal(t, errors.EBadRequest, err.Code())
	assert.Equal(t, "sample test", err.Error())

	err2 := errors.NewErrorf(errors.EBadRequest, e, "sample %s", "test")
	_, ok = err2.(error)
	assert.True(t, ok)
	assert.Equal(t, e, err2.Cause())
	assert.Equal(t, errors.EBadRequest, err2.Code())
	assert.Equal(t, "sample test", err2.Error())
}

func TestUnexpectedError(t *testing.T) {
	var ok bool
	e01 := fmt.Errorf("basic error")

	err := errors.NewUnexpectedError("sample error", e01)
	_, ok = err.(error)
	assert.True(t, ok)
	assert.Equal(t, e01, err.Cause())
	assert.Equal(t, errors.EUnexpected, err.Code())
	assert.Equal(t, "sample error", err.Error())
}
