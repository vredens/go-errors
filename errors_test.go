package errors_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	errors "gitlab.com/vredens/go-errors"
)

func Example() {
	errors.New("error")
	errors.WithCode(errors.EDevPoo).New("something")

	var err1 = fmt.Errorf("root error")
	var err2 = errors.Wrap(err1, "some other error")
	var err3 = errors.WithCode(errors.EBufferFull).Wrap(err2, "fail to open")

	switch errors.CodeOf(err3) {
	case errors.EBufferFull:
		// we handle buffer fulls differently
	case 10:
		// we also have some weird error code we should probably convert to a constant
	default:
		// something unexpected happened
	}

	// both these lines do the same
	fmt.Printf("error: %+v\n", err3)
	fmt.Printf("error: %s\n", errors.Collate(err3)) // works for any type of error
}

func TestWithCode(t *testing.T) {
	var ok bool
	e := fmt.Errorf("basic error")
	enc := errors.WithCode(errors.EBadRequest)

	err := enc.New("one")
	_, ok = err.(error)
	assert.Equal(t, true, ok)
	assert.Nil(t, err.Cause())
	assert.Equal(t, errors.EBadRequest, err.Code())

	err = enc.Wrap(e, "one")
	_, ok = err.(error)
	assert.Equal(t, true, ok)
	assert.Equal(t, e, err.Cause())
	assert.Equal(t, errors.EBadRequest, err.Code())
}

type anotherErrorType struct{}

func (e *anotherErrorType) GetCode() int  { return errors.EBadRequest }
func (e *anotherErrorType) Error() string { return "same old same old" }

func TestCodeOf(t *testing.T) {
	e01 := fmt.Errorf("error 01")
	e02 := errors.New("error 02")
	e03 := errors.WithCode(errors.EBufferFull).New("error 03")
	e04 := errors.WithCode(errors.EDevPoo).Wrap(e01, "error 04")
	e05 := errors.Wrap(e03, "error 05")
	e06 := &anotherErrorType{}

	assert.Equal(t, errors.EUnexpected, errors.CodeOf(e01))
	assert.Equal(t, errors.EUnexpected, errors.CodeOf(e02))
	assert.Equal(t, errors.EBufferFull, errors.CodeOf(e03))
	assert.Equal(t, errors.EDevPoo, errors.CodeOf(e04))
	assert.Equal(t, errors.EUnexpected, errors.CodeOf(e05))
	assert.Equal(t, errors.EBadRequest, errors.CodeOf(e06))

}

func TestNew(t *testing.T) {
	var ok bool

	e01 := errors.New("sample %s", "test")
	_, ok = e01.(error)
	assert.True(t, ok)
	assert.Nil(t, e01.Cause())
	assert.Equal(t, errors.EUnexpected, e01.Code())
	assert.Equal(t, "sample test", e01.Error())

	e02 := errors.New("sample test")
	_, ok = e02.(error)
	assert.True(t, ok)
	assert.Nil(t, e02.Cause())
	assert.Equal(t, errors.EUnexpected, e02.Code())
	assert.Equal(t, "sample test", e02.Error())
}

func TestWrap(t *testing.T) {
	var ok bool

	err := fmt.Errorf("base")
	e01 := errors.Wrap(err, "sample %s", "test")
	_, ok = e01.(error)
	assert.True(t, ok)
	assert.Equal(t, err, e01.Cause())
	assert.Equal(t, errors.EUnexpected, e01.Code())
	assert.Equal(t, "sample test", e01.Error())

	e02 := errors.Wrap(e01, "sample test")
	_, ok = e02.(error)
	assert.True(t, ok)
	assert.Equal(t, e01, e02.Cause())
	assert.Equal(t, errors.EUnexpected, e02.Code())
	assert.Equal(t, "sample test", e02.Error())
}

func TestAssinability(t *testing.T) {
	var err errors.Error

	err = errors.New("sample error")
	assert.NotNil(t, err)

	err = errors.Wrap(errors.New("base"), "sample error")
	assert.NotNil(t, err)
}

func TestPrintf(t *testing.T) {
	var err = errors.WithCode(errors.EBadRequest).Wrap(fmt.Errorf("core error"), "invalid parameter dummy")
	assert.Equal(t, err.Error(), "invalid parameter dummy")
	assert.Equal(t, fmt.Sprintf("%+v", err), "invalid parameter dummy; core error")
	assert.Equal(t, fmt.Sprintf("%v", err), "invalid parameter dummy; core error")
	assert.Equal(t, fmt.Sprintf("%s", err), "invalid parameter dummy")
	assert.Equal(t, fmt.Sprintf("%q", err), `"invalid parameter dummy; core error"`)

}

func ExampleEncoder() {
	// this should probably be a global
	BadRequest := errors.WithCode(errors.EBadRequest)

	// a dummy validator function
	validator := func() error {
		return fmt.Errorf("powerOfTheDarkSide parameter must be undefined")
	}

	// call the validator and wrap any errors within a bad request error
	err := validator()
	if err != nil {
		err = BadRequest.Wrap(err, "error ocurred validating The Force™")
	}

	if err != nil {
		switch err.(errors.Error).Code() {
		case errors.EBadRequest:
			// do something
		default:
			// panic!
		}
	}
}

func ExampleWrap() {
	err := errors.WithCode(errors.EBadRequest).Wrap(fmt.Errorf("core error"), "invalid parameter dummy")

	fmt.Println(errors.Collate(err))
	// Output: invalid parameter dummy; core error
}
