.PHONY: test build proto

config:
	dep ensure

test:
	go test -v -race ./...

bench:
	go test -v -tags bench -bench Bench ./...

coverage:
	go test -v -coverprofile=.coverage.out -timeout 30s ./...
	go tool cover -func=.coverage.out

install:
	go get -u ./...
